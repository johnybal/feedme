package org.johny.feedme;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by johnybalian on 2017-02-21.
 */

public class FeedMeApplication extends Application {
    public void onCreate() {
        Fabric.with(this, new Crashlytics());
        //testing jenkins
    }
}