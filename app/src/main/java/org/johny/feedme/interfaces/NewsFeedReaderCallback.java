package org.johny.feedme.interfaces;

import org.johny.feedme.models.Feed;

/**
 * Created by johnybalian on 2016-10-17.
 */

public interface NewsFeedReaderCallback {
    void onLoadComplete(Feed feed);
}
