package org.johny.feedme.models;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by johnybalian on 2016-10-17.
 */

public class FeedImage implements Serializable {
    private String imageUrl;
    private int width;
    private int height;
    private String alt;
    private String title;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setWidth(String width) {
        if (!TextUtils.isEmpty(width)) {
            this.width = Integer.parseInt(width);
        }
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setHeight(String height) {
        if (!TextUtils.isEmpty(height)) {
            this.height = Integer.parseInt(height);
        }
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
