package org.johny.feedme.models;

import android.graphics.drawable.ColorDrawable;

import org.johny.feedme.services.FeedReader;

import java.io.Serializable;
import java.util.List;

/**
 * Created by johnybalian on 2016-10-17.
 */

public class Feed implements Serializable {
    private List<FeedItem> feedItemList;
    private String feedName;
    private String feedUrl;
    private ColorDrawable feedColor;
    private FeedReader feedReader;

    public List<FeedItem> getFeedItemList() {
        return feedItemList;
    }

    public void setFeedItemList(List<FeedItem> feedItemList) {
        this.feedItemList = feedItemList;
    }

    public String getFeedName() {
        return feedName;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    public String getFeedUrl() {
        return feedUrl;
    }

    public void setFeedUrl(String feedUrl) {
        this.feedUrl = feedUrl;
    }

    public ColorDrawable getFeedColor() {
        return feedColor;
    }

    public void setFeedColor(ColorDrawable feedColor) {
        this.feedColor = feedColor;
    }

    public FeedReader getFeedReader() {
        return feedReader;
    }

    public void setFeedReader(FeedReader feedReader) {
        this.feedReader = feedReader;
    }
}
