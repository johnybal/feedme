package org.johny.feedme.models;

import java.io.Serializable;

/**
 * Created by johnybalian on 2016-10-17.
 */

public class FeedItem implements Serializable{

    private String title;
    private String storyUrl;
    private String description;
    private String publicationDate;
    private String author;
    private FeedImage feedImage;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStoryUrl() {
        return storyUrl;
    }

    public void setStoryUrl(String storyUrl) {
        this.storyUrl = storyUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public FeedImage getFeedImage() {
        return feedImage;
    }

    public void setFeedImage(FeedImage feedImage) {
        this.feedImage = feedImage;
    }
}
