package org.johny.feedme.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;

import org.johny.feedme.R;
import org.johny.feedme.utils.Utils;

import io.fabric.sdk.android.Fabric;

/**
 * Created by jbalian on 2016-10-23.
 */

public class BaseActivity extends AppCompatActivity {

    protected Toolbar mToolbar;
    protected int mToolbarColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        updateToolbarColor(mToolbarColor);

    }

    protected void updateToolbar(String title) {
        updateToolbar(title, -1);
    }

    protected void updateToolbar(String title, int color) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        if (color != -1) {
            updateToolbarColor(color);
        }
    }

    protected void updateToolbarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && color != -1) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Utils.darker(color, 1 - 0.2F));
            mToolbar.setBackgroundColor(color);
        }
    }
}
