package org.johny.feedme.activities;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.johny.feedme.R;
import org.johny.feedme.adapters.NewsFeedAdapter;
import org.johny.feedme.controllers.NewsFeedController;
import org.johny.feedme.interfaces.ItemClickSupport;
import org.johny.feedme.models.Feed;
import org.johny.feedme.services.CBCFeedReader;
import org.johny.feedme.services.DailyStarFeedReader;
import org.johny.feedme.services.NaharnetFeedReader;
import org.johny.feedme.viewaccessors.NewsFeedViewAccessor;

import java.util.ArrayList;
import java.util.List;

public class NewsFeedActivity extends BaseActivity implements NewsFeedViewAccessor {

    private final String SAVE_INSTANCE_NEWSFEED = "SAVED_FEED";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private NewsFeedController mNewsFeedController;
    private TextView mEmptyView;
    private Feed mNewsFeedData;
    private boolean mIsConfigChange;

    private Drawer mDrawer;

    private List<Feed> mFeeds;
    private Feed mSelectedFeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_newsfeed);
        if (savedInstanceState != null) {
            mIsConfigChange = true;
        }
        updateToolbar(getResources().getString(R.string.title_newsfeed));
        initFeeds();
        initMenuDrawer();
        bindViews();
        init();
    }

    SwipeRefreshLayout.OnRefreshListener swipeRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            requestNewsFeed(mSelectedFeed);
        }
    };

    private void initFeeds() {
        mFeeds = new ArrayList<>();

        Feed cbc = new Feed();
        cbc.setFeedName("CBC News");
        cbc.setFeedUrl("http://www.cbc.ca/cmlink/rss-topstories");
        cbc.setFeedColor(new ColorDrawable(ContextCompat.getColor(this, R.color.md_red_500)));
        cbc.setFeedReader(new CBCFeedReader(cbc.getFeedUrl()));
        mFeeds.add(cbc);

        Feed dailystar = new Feed();
        dailystar.setFeedName("Daily Star Lebanon");
        dailystar.setFeedUrl("http://www.dailystar.com.lb/RSS.aspx?id=1");
        dailystar.setFeedColor(new ColorDrawable(ContextCompat.getColor(this, R.color.md_indigo_500)));
        dailystar.setFeedReader(new DailyStarFeedReader(dailystar.getFeedUrl()));
        mFeeds.add(dailystar);

        Feed naharnet = new Feed();
        naharnet.setFeedName("Naharnet");
        naharnet.setFeedUrl("http://www.naharnet.com/tags/lebanon/en/feed.atom");
        naharnet.setFeedColor(new ColorDrawable(ContextCompat.getColor(this, R.color.md_green_800)));
        naharnet.setFeedReader(new NaharnetFeedReader(naharnet.getFeedUrl()));
        mFeeds.add(naharnet);
    }

    private void initMenuDrawer() {
        PrimaryDrawerItem addNewFeed = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.add_new_feed);

        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolbar)
                .addDrawerItems(
                        addNewFeed,
                        new DividerDrawerItem()
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        int identifier = (int) drawerItem.getIdentifier();
                        if (identifier >= 2) {
                            mSelectedFeed = mFeeds.get(identifier - 2);
                            updateToolbar(mSelectedFeed.getFeedName(), mSelectedFeed.getFeedColor().getColor());
                            requestNewsFeed(mSelectedFeed);
                        }
                        return true;
                    }
                })
                .build();

        int counter = 2;
        for (Feed feed : mFeeds) {
            mDrawer.addItem(new SecondaryDrawerItem().withIdentifier(counter++).withName(feed.getFeedName()));
        }
    }

    private void bindViews() {
        //we can use butterknife...
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mEmptyView = (TextView) findViewById(R.id.empty_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
    }

    private void init() {
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new NewsFeedAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                String url = ((NewsFeedAdapter) mAdapter).getFeedItemUrl(position);
                int color = mSelectedFeed.getFeedColor().getColor();
                openNewsFeed(url, color);
            }
        });

        mNewsFeedController = new NewsFeedController(this);
        mSwipeRefreshLayout.setOnRefreshListener(swipeRefreshListener);

        if (!mIsConfigChange) {
            mDrawer.setSelection(2);
        }
    }

    private void openNewsFeed(String url, int color) {
        startActivity(WebviewActivity.createWebviewLauncherIntent(this, url, color));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void requestNewsFeed(Feed selectedFeed) {
        mNewsFeedController.loadNewsItems(this, selectedFeed);
        if (mDrawer != null) {
            mDrawer.closeDrawer();
        }
    }

    @Override
    public void onNewsFeedDataConverted(Feed rssFeed) {
        if (rssFeed == null || rssFeed.getFeedItemList() == null || rssFeed.getFeedItemList().isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
            Toast.makeText(this, R.string.error_loading_feed, Toast.LENGTH_LONG).show();
        } else {
            mNewsFeedData = rssFeed;

            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
            ((NewsFeedAdapter) mAdapter).update(rssFeed.getFeedItemList());
            mAdapter.notifyDataSetChanged();
        }

        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //save the news feed
        outState.putSerializable(SAVE_INSTANCE_NEWSFEED, mNewsFeedData);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mNewsFeedData = (Feed) savedInstanceState.getSerializable(SAVE_INSTANCE_NEWSFEED);
        onNewsFeedDataConverted(mNewsFeedData);

    }
}
