package org.johny.feedme.controllers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.johny.feedme.interfaces.NewsFeedReaderCallback;
import org.johny.feedme.models.Feed;
import org.johny.feedme.services.NewsFeedReaderService;
import org.johny.feedme.viewaccessors.NewsFeedViewAccessor;

/**
 * Created by johnybalian on 2016-10-17.
 */

public class NewsFeedController implements NewsFeedReaderCallback {


    private NewsFeedViewAccessor viewAccessor;

    public NewsFeedController(NewsFeedViewAccessor viewAccessor){
        this.viewAccessor = viewAccessor;
    }

    /**
     * Checks if we have a valid connection before calling the service, otherwise it calls
     * onLoadComplete(null)
     */
    public void loadNewsItems(Context context, Feed selectedFeed){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        //check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            new NewsFeedReaderService(this).execute(selectedFeed);
        }else{
            onLoadComplete(null);
        }
    }

    /**
     * @param feed the news feed
     */
    @Override
    public void onLoadComplete(Feed feed) {
        if(viewAccessor != null) viewAccessor.onNewsFeedDataConverted(feed);
    }
}
