package org.johny.feedme.viewaccessors;

import org.johny.feedme.models.Feed;

/**
 * Created by johnybalian on 2016-10-17.
 *
 * This interface is to be implemented by anyone using the NewsFeedController
 */

public interface NewsFeedViewAccessor {
    /**
     * The rssFeed is null if an error occured,
     * @param rssFeed
     */
    void onNewsFeedDataConverted(Feed rssFeed);
}
