package org.johny.feedme.services;

import android.os.AsyncTask;

import org.johny.feedme.interfaces.NewsFeedReaderCallback;
import org.johny.feedme.models.Feed;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by johnybalian on 2016-10-17.
 */

public class NewsFeedReaderService extends AsyncTask<Feed, Void, Feed> {


    private NewsFeedReaderCallback listener;

    public NewsFeedReaderService(NewsFeedReaderCallback listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Feed doInBackground(Feed... params) {

        Feed feed = null;
        try {
            feed = params[0];
            feed = feed.getFeedReader().getFeed();
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
        return feed;
    }

    protected void onPostExecute(Feed result) {
        listener.onLoadComplete(result);
    }


}
