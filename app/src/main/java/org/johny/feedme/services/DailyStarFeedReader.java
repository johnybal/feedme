package org.johny.feedme.services;

import org.johny.feedme.models.Feed;
import org.johny.feedme.models.FeedItem;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by johnybalian on 2016-10-18.
 */

public class DailyStarFeedReader extends FeedReader {

    public DailyStarFeedReader(String url){
        super(url);
    }

    @Override
    protected Feed parseXML(InputStream is) throws XmlPullParserException, IOException {
        Feed feed = new Feed();
        List<FeedItem> feedItems = new ArrayList<>();

        XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
        XmlPullParser parser = xmlFactoryObject.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(is, null);

        int eventType = parser.getEventType();
        FeedItem feedItem = null;
        while (eventType != XmlPullParser.END_DOCUMENT) {


            if (eventType == XmlPullParser.START_TAG) {
                String tag = parser.getName();
                switch (tag) {
                    case "item":
                        feedItem = new FeedItem();
                        break;
                    case "title":
                        parser.next();
                        if (feedItem != null) feedItem.setTitle(parser.getText());
                        break;
                    case "link":
                        parser.next();
                        if (feedItem != null) feedItem.setStoryUrl(parser.getText());
                        break;
                    case "description":
                        parser.next();
                        if (feedItem != null) feedItem.setDescription(parser.getText());
                        break;
                    case "pubDate":
                        parser.next();
                        if (feedItem != null) feedItem.setPublicationDate(parser.getText());
                        break;
                    case "author":
                        parser.next();
                        if (feedItem != null) feedItem.setAuthor(parser.getText());
                        break;
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                String tag = parser.getName();
                if (tag.equals("item")) {
                    feedItems.add(updateUrl(feedItem));
                }
            }
            eventType = parser.next();

        }

        feed.setFeedItemList(feedItems);

        return feed;
    }

    private FeedItem updateUrl(FeedItem feedItem){
        String oldUrl = feedItem.getStoryUrl();
        String[] tokens = oldUrl.split("/");
        int last = tokens.length;

        String newUrl = "http://www.dailystar.com.lb/GetArticleBody.aspx?id="+tokens[last-1].split("-")[0]+"&fromgoogle=1";
        feedItem.setStoryUrl(newUrl);
        return feedItem;

    }
}
