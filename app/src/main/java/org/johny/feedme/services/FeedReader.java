package org.johny.feedme.services;

import org.johny.feedme.models.Feed;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by johnybalian on 2016-10-17.
 */

public abstract class FeedReader {

    private static final int READ_TIMEOUT_MS = 10000;
    private static final int CONNECTIION_TIMEOUT_MS = 15000;
    private URL mUrl;


    /**
     * @param url The feeds url
     */
    FeedReader(String url){
        try {
            this.mUrl = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens an InputStream on the feeds url.
     *
     * @return InputStream
     * @throws IOException
     * @throws XmlPullParserException
     */
    private InputStream fetchXML() throws IOException, XmlPullParserException {
        HttpURLConnection conn = (HttpURLConnection) mUrl.openConnection();

        conn.setReadTimeout(READ_TIMEOUT_MS);
        conn.setConnectTimeout(CONNECTIION_TIMEOUT_MS);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);

        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }

    /**
     * I made this abstract because I noticed that each feed has special ways to parse it,
     * the CBC one embeds info in the description tag while others put them elsewhere!
     *
     * @param is The RSS input stream that needs to be parsed
     * @return Feed object which contains a List<FeedItem>
     * @throws XmlPullParserException
     * @throws IOException
     */
    protected abstract Feed parseXML(InputStream is) throws XmlPullParserException, IOException;


    /**
     * Opens an InputStream, parses it and then closes the stream, if needed, we can optimize by
     * streaming back the result...
     *
     * @return Feed object which contains a List<FeedItem>
     * @throws IOException
     * @throws XmlPullParserException
     */
    public Feed getFeed() throws IOException, XmlPullParserException {
        InputStream inputStream = fetchXML();
        Feed feed = parseXML(inputStream);
        inputStream.close();
        return feed;
    }
}
