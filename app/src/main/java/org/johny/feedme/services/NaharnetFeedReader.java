package org.johny.feedme.services;

import android.text.TextUtils;

import org.johny.feedme.models.Feed;
import org.johny.feedme.models.FeedImage;
import org.johny.feedme.models.FeedItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jbalian on 2016-10-25.
 */

public class NaharnetFeedReader extends FeedReader {

    public NaharnetFeedReader(String url) {
        super(url);
    }

    @Override
    protected Feed parseXML(InputStream is) throws XmlPullParserException, IOException {
        Feed feed = new Feed();
        List<FeedItem> feedItems = new ArrayList<>();

        XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
        XmlPullParser parser = xmlFactoryObject.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(is, null);

        int eventType = parser.getEventType();
        FeedItem feedItem = null;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                String tag = parser.getName();
                switch (tag) {
                    case "entry":
                        feedItem = new FeedItem();
                        break;
                    case "title":
                        parser.next();
                        if (feedItem != null) feedItem.setTitle(parser.getText());
                        break;
                    case "link":
                        parser.next();
                        if (feedItem != null) {
                            String url = parser.getAttributeValue(null, "href");
                            if (url != null) {
                                url = url.replace("http://www.", "http://m.");
                                feedItem.setStoryUrl(url);
                            }
                        }
                        break;
                    case "summary":
                        parser.next();
                        if (feedItem != null) feedItem.setDescription(parser.getText());
                        break;
                    case "published":
                        parser.next();
                        if (feedItem != null) feedItem.setPublicationDate(parser.getText());
                        break;
                    case "author":
                        parser.next();
//                        if (feedItem != null) feedItem.setAuthor(parser.getText());
                        break;
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                String tag = parser.getName();
                if (tag.equals("entry")) {
                    feedItems.add(cleanup(feedItem));
                }
            }
            eventType = parser.next();
        }

        feed.setFeedItemList(feedItems);

        return feed;
    }

    private FeedItem cleanup(FeedItem feedItem) {
        if (feedItem != null && !TextUtils.isEmpty(feedItem.getDescription())) {

            Document document = Jsoup.parse(feedItem.getDescription());
            Elements img = document.select("img");
            Elements desc = document.select("p");

            if (img != null && img.first() != null) {
                FeedImage feedImage = new FeedImage();
                feedImage.setImageUrl(img.first().attr("src"));
                feedImage.setAlt(img.first().attr("alt"));
                feedImage.setTitle(img.first().attr("title"));
                feedImage.setWidth(img.first().attr("width"));
                feedImage.setHeight(img.first().attr("height"));

                feedItem.setFeedImage(feedImage);
            }
            if (desc != null && desc.first() != null) {
                feedItem.setDescription(desc.first().text());
            }

            //it's always the same
            feedItem.setAuthor("Naharnet Newsdesk");
        }
        return feedItem;
    }
}
