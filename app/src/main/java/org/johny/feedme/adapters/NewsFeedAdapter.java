package org.johny.feedme.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.johny.feedme.R;
import org.johny.feedme.models.FeedImage;
import org.johny.feedme.models.FeedItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by johnybalian on 2016-10-17.
 */

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.ViewHolder> {

    private List<FeedItem> mDataset;
    private Context context;


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, date, author;
        ImageView feedImage;

        ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);
            author = (TextView) view.findViewById(R.id.author);
            feedImage = (ImageView) view.findViewById(R.id.imageView);
        }
    }

    public NewsFeedAdapter(Context context) {
        this.context = context;
        this.mDataset = new ArrayList<>();
    }

    public void update(List<FeedItem> feedItems) {
        this.mDataset = feedItems;
    }

    public String getFeedItemUrl(int position) {
        return mDataset.get(position).getStoryUrl();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NewsFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rss_list_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FeedItem feedItem = mDataset.get(position);
        holder.title.setText(feedItem.getTitle());
        holder.date.setText(feedItem.getPublicationDate());
        holder.author.setText(feedItem.getAuthor());
        FeedImage feedImage = feedItem.getFeedImage();
        if(feedImage != null && feedImage.getImageUrl() != null){
            Picasso.with(context).load(feedImage.getImageUrl()).centerCrop()
                    .resize(160, 160).placeholder(R.drawable.ic_noimage).error(R.drawable.ic_noimage).into(holder.feedImage);
            if(feedImage.getAlt() != null) {
                holder.feedImage.setContentDescription(feedImage.getAlt());
            }
        }else{
            //its very quick to load this resource, we don't need to do anything special
            holder.feedImage.setImageResource(R.drawable.ic_noimage);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
